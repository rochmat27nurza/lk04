import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program Pembagian Bilangan");

        boolean validInput = false;
        do {
            //edit here
            try{
                System.out.print("Masukkan Pembilang : ");
                int pembilang = scanner.nextInt();

                System.out.print("Masukkan penyebut : ");
                int penyebut = scanner.nextInt();

                int hasil = pembagian(pembilang, penyebut);
                System.out.println("Hasil : " + hasil);
                validInput = true;
            }
            catch(InputMismatchException e){
                System.out.println("Your input not valid. Must a integer!");
                scanner.nextLine();
            } catch (Exception e){
                System.out.println(e.getMessage());
            }

        } while (!validInput);

        scanner.close();
    }

    public static int pembagian(int pembilang, int penyebut) {
        //add exception apabila penyebut bernilai 0
        if(penyebut == 0 ) {
            throw new ArithmeticException("your input is wrong. 0 haram bro");
        }
        return pembilang / penyebut;
    }
}
